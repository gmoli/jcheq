# Autor: Jorge Sivil <jsivil@unpaz.edu.ar>
# Referencia: https://files.support.epson.com/pdf/general/escp2ref.pdf


class PrintDriver:

    @staticmethod
    def process(raw_data, params={}):
        raise NotImplementedError


class EscP(PrintDriver):

    def __init__(self):
        pass

    # Se asumen cheques de altura 75mm, se utilizan 8 lineas por pulgada, resultando en 24 lineas por cheque.
    @staticmethod
    def process(raw_data, params={}):
        data = b''
        data += b'\x1B\x40'             # Initialize printer (ESC @)
        data += b'\x1B\x50\x1B\x0F'     # 10.5 point, 10 cpi pitch (ESC P) + Condensed Mode (ESC SI), para tener un cpi de 17.14.
        data += b'\x1B\x30'             # Selects 1/8-inch line spacing (8 lines per inch)
        data += b'\x1B\x46'             # Sets font weight normal

        datos_impresion = []

        for data_part in raw_data:
            linea_impresion = {'num_linea': data_part['y'], 'dato': b''}

            num_columna = data_part['x']
            valor = str(data_part['val'])

            if len(valor) != 0:
                linea_impresion['dato'] = b' ' * num_columna   # Rellenar espacios hasta col-1 (0-based)
                linea_impresion['dato'] += valor.encode('latin1')

            datos_impresion.append(linea_impresion)

        for i in range(0, 24):
            for j in range(0, len(datos_impresion)):
                if i == datos_impresion[j]['num_linea']:
                    data += datos_impresion[j]['dato'] + b'\x0D'  # Carriage return
            data += b'\x0A'  # Line feed

        if 'linefeed_qty' in params:
            for i in range(0, params['linefeed_qty']):
                data += b'\x0A'

        if 'eject' in params and params['eject']:
            data += b'\x0C'

        data += b'\x1B\x40'             # Reset printer (ESC @)
        return data

