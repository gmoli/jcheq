# Autor: Jorge Sivil <jsivil@unpaz.edu.ar>
import os
import tempfile
import subprocess
import shutil

from logger import logger


class PrintSystem:
    pass


class WinLPR(PrintSystem):
    """
        WinLPR utiliza el programa 'lpr' de Windows. Imprime directo sobre la impresora, por TCP/IP. No requiere que la
        impresora sea conocida por el sistema local.
    """

    def __init__(self):
        pass

    @staticmethod
    def send(spool, params):
        Helper.log_print_system_info('WinLPR', params)

        temp_dir = tempfile.mkdtemp()
        try:
            Helper.log_print_system_usage('Creando archivo temporal en dir: ' + temp_dir, logger.Logger.TRACE)
            with tempfile.NamedTemporaryFile(dir=temp_dir, delete=False) as temp_file:
                temp_file.write(spool.data)
            output = subprocess.check_output(['lpr', '-S', params['ip'], '-P', params['queue'], '-J', spool.title, '-o l', temp_file.name], stderr=subprocess.STDOUT)
            Helper.log_print_system_usage(b'WinLPR output: ' + output, logger.Logger.TRACE)
        except subprocess.CalledProcessError as e:
            Helper.log_print_system_usage(e.output, logger.Logger.ERROR)
            raise e
        finally:
            shutil.rmtree(temp_dir)


class LPR(PrintSystem):
    """
        LPR utiliza el programa 'lpr'. Este a su vez utiliza CUPS, asique ambos deben estar instalados.
        La impresora debe ser conocida al sistema local, es decir, debe estar configurada.
    """

    def __init__(self):
        pass

    @staticmethod
    def send(spool, params):
        Helper.log_print_system_info('LPR', params)

        try:
            output = subprocess.check_output(['lpr', '-H', params['ip'], '-P', params['queue'], '-J', spool.title], input=spool.data, stderr=subprocess.STDOUT)
            if output != '':
                Helper.log_print_system_usage(b'LPR output: ' + output, logger.Logger.TRACE)
        except subprocess.CalledProcessError as e:
            Helper.log_print_system_usage(e.output, logger.Logger.ERROR)
            raise e


class RLPR(PrintSystem):
    """
        RLPR utiliza el programa 'rlpr'. Imprime directo sobre la impresora, por TCP/IP. No requiere que la
        impresora sea conocida por el sistema local.
    """

    def __init__(self):
        pass

    @staticmethod
    def send(spool, params):
        Helper.log_print_system_info('RLPR', params)

        try:
            output = subprocess.check_output(['rlpr', '-H' + params['ip'], '-P' + params['queue'], '-T' + spool.title], input=spool.data, stderr=subprocess.STDOUT)
            if output != '':
                Helper.log_print_system_usage(b'RLPR output: ' + output, logger.Logger.TRACE)
        except subprocess.CalledProcessError as e:
            Helper.log_print_system_usage(e.output, logger.Logger.ERROR)
            raise e


class NetCat(PrintSystem):
    """
        NetCat utiliza el programa 'nc'. Este programa abre conexiones arbitrarias a una IP en un puerto. Generalmente
        las impresoras con puerto de red implementan el puerto 9100 para recibir información tipo raw.
        Esta clase, así como está, no suele funcionar en printservers.
    """

    def __init__(self):
        pass

    @staticmethod
    def send(spool, params):
        Helper.log_print_system_info('NetCat', params)

        try:
            output = subprocess.check_output(['nc', params['ip'], '9100'], input=spool.data, stderr=subprocess.STDOUT)
            if output != '':
                Helper.log_print_system_usage(b'NetCat output: ' + output, logger.Logger.TRACE)
        except subprocess.CalledProcessError as e:
            Helper.log_print_system_usage(e.output, logger.Logger.ERROR)
            raise e


class Helper:
    def __init__(self):
        pass

    @staticmethod
    def log_print_system_info(print_system_name, params):
        Helper.log_print_system_usage('Sistema de Impresión: ' + print_system_name, logger.Logger.DEBUG)
        Helper.log_print_system_usage('Parámetros: ' + str(params), logger.Logger.DEBUG)

    @staticmethod
    def log_print_system_usage(msg, loglevel):
        parent_dir = os.path.dirname(os.path.realpath(__file__ + '/..'))
        logger_i = logger.LoggerFactory.get_logger(parent_dir + '/logs/jcheq.log')
        logger_i.log(msg, loglevel)
